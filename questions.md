# Assignment 4: Models

The purpose of this assignment is to let you experiment with models we
introduced in class: linear regression, mixture, and Gaussian process.
Submission deadline is Tue, July 14, 2020, 23:59 AOE.

This assignment contains two questions. For each question, you are
given a data set and asked to perform an analysis on the data. The
analysis should include all usual steps, as we learned during the
course:

1. Initial data exploration
2. Specification of the initial model
3. Inference of the posterior
4. Predictive posterior checks and sensitivity analysis
5. Expansion of the model and evaluation of the expanded model

It is sufficient to answer one question out of the two for the
full grade. If you submit answers to both question, you may
get up to 40 points bonus for the homework (up to 5 points bonus
for the course).

You may use any Bayesian inference framework of your choice
(Infergo, Stan, Pyro, Anglican, PyMC3, Turing,jl, Edward, ...)
to solve either question.

Question 1: Unsupervised exploration of Iris dataset

[Iris flower data set](https://en.wikipedia.org/wiki/Iris_flower_data_set)
contains measurements of features of three related specifes of Iris flowers.
How well do the features separate species? Your model should use only the
features, not the species labels, for inference. However, you may use species
labels to assess the informativeness of the features to identify a species.
For your convenience, the data set is ./datasets/iris.csv in the course
repository.

Question 2: Corona in Israel

The Ministry of Health reports the number of tests and the number of positive
results every day since mid-March. The data set (thanks to Igor Vynokur for
finding and retrieving the data) is ./datasets/corona.csv in
the course repository. What is your prognosis on the total
number of cases in the population, on each day? What percentage
of tests will come out positive one day, seven days, fourteen
days after the last data point in the data set?