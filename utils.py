import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pymc3 as pm
import matplotlib

sns.set()
plt.rcParams["figure.figsize"] = (10.0, 8.0)


def plot_hist(param, param_name="parameter"):
    """Plot the posterior of a parameter."""

    # Summary statistics
    mean = np.mean(param)
    median = np.median(param)
    cred_min, cred_max = np.percentile(param, 2.5), np.percentile(param, 97.5)

    plt.title(f"Distribution for {param_name}")
    # Plotting
    plt.hist(param, 30, density=True)
    sns.kdeplot(param, shade=True)
    plt.xlabel(param_name)
    plt.ylabel("density")
    plt.axvline(mean, color="r", lw=2, linestyle="--", label=f"mean: {mean:.2f}")
    plt.axvline(median, color="c", lw=2, linestyle="--", label="median")
    plt.axvline(
        cred_min,
        linestyle=":",
        color="k",
        alpha=0.2,
        label=f"95% CI: ({cred_min:.2f},{cred_max:.2f})",
    )
    plt.axvline(cred_max, linestyle=":", color="k", alpha=0.2)

    plt.gcf().tight_layout()
    plt.legend()


def plot_2_hist(params, params_names="parameter"):
    """Plot the posterior of a parameter."""

    # Summary statistics
    mean = np.mean(params, axis=1)
    # median = np.median(params, axis=1)
    cred_min, cred_max = (
        np.percentile(params, 2.5, axis=1),
        np.percentile(params, 97.5, axis=1),
    )

    plt.title(f"Distribution for {params_names[0]}, {params_names[1]}")
    # Plotting
    plt.hist(params[0], 30, density=True, label=params_names[0], alpha=0.5)
    sns.kdeplot(params[0], shade=True, color="b")

    plt.hist(params[1], 30, density=True, label=params_names[1], alpha=0.5)
    sns.kdeplot(params[1], shade=True, color="g")

    plt.xlabel("params values")
    plt.ylabel("density")
    plt.axvline(
        mean[0],
        color="b",
        lw=2,
        label=f"control mean: {mean[0]:.2f}"
        # mean[0], color="r", lw=2, linestyle="--", label=f"control mean: {mean[0]:.2f}"
    )
    plt.axvline(
        mean[1],
        color="g",
        lw=2,
        label=f"treatment mean: {mean[1]:.2f}"
        # mean[1], color="r", lw=2, linestyle="--", label=f"treatment mean: {
        # mean[1]:.2f}"
    )
    # plt.axvline(median[0], color="b", lw=2, linestyle="--", label="control median")
    # plt.axvline(median[1], color="g", lw=2, linestyle="--", label="treatment median")
    plt.axvline(
        cred_min[0],
        linestyle=":",
        color="k",
        alpha=0.2,
        label=f"control 95% CI: ({cred_min[0]:.2f},{cred_max[0]:.2f})",
    )
    plt.axvline(
        cred_min[1],
        linestyle=":",
        color="k",
        alpha=0.2,
        label=f"treatment 95% CI: ({cred_min[1]:.2f},{cred_max[1]:.2f})",
    )
    plt.axvline(cred_max[0], linestyle=":", color="k", alpha=0.2)
    plt.axvline(cred_max[1], linestyle=":", color="k", alpha=0.2)

    plt.gcf().tight_layout()
    plt.legend()


def plot_trace(param, param_name="parameter"):
    """Plot the trace and posterior of a parameter."""

    # Summary statistics
    mean = np.mean(param)
    median = np.median(param)
    cred_min, cred_max = np.percentile(param, 2.5), np.percentile(param, 97.5)

    # Plotting
    plt.subplot(2, 1, 1)
    plt.plot(param)
    plt.xlabel("samples")
    plt.ylabel(param_name)
    plt.axhline(mean, color="r", lw=2, linestyle="--")
    plt.axhline(median, color="c", lw=2, linestyle="--")
    plt.axhline(cred_min, linestyle=":", color="k", alpha=0.2)
    plt.axhline(cred_max, linestyle=":", color="k", alpha=0.2)
    plt.title("Trace and Posterior Distribution for {}".format(param_name))

    plt.subplot(2, 1, 2)
    plt.hist(param, 30, density=True)
    sns.kdeplot(param, shade=True)
    plt.xlabel(param_name)
    plt.ylabel("density")
    plt.axvline(mean, color="r", lw=2, linestyle="--", label="mean")
    plt.axvline(median, color="c", lw=2, linestyle="--", label="median")
    plt.axvline(cred_min, linestyle=":", color="k", alpha=0.2, label="95% CI")
    plt.axvline(cred_max, linestyle=":", color="k", alpha=0.2)

    plt.gcf().tight_layout()
    plt.legend()


def plot_predictions(
    x,
    y,
    predictions,
    x_future=None,
    y_future=None,
    show_gp=False,
    figsize=(6, 6),
    title=None,
):
    plt.figure(figsize=figsize)
    if title is None:
        title = "Posterior Prediction Visualization"
    plt.title(title)
    plt.ylim(0, 0.1)
    color_scatter = "C0"  # 'C0' for default color #0
    color_line = "C3"  # 'C1' for default color #1
    plot_future = x_future is not None and y_future is not None
    if show_gp:
        samples = predictions.shape[0]
        for p in predictions:
            plt.plot(x, p, color="firebrick", alpha=10 / samples)

        if plot_future:
            for p in y_future:
                plt.plot(x_future, p, color="firebrick", alpha=10 / samples)

    else:
        plt.fill_between(
            x,
            np.percentile(predictions, 5, axis=0),
            np.percentile(predictions, 95, axis=0),
            color=1 - 0.4 * (1 - np.array(matplotlib.colors.to_rgb(color_line))),
            label="95% CI",
        )
        plt.plot(
            x,
            np.percentile(predictions, 50, axis=0),
            color=color_line,
            linewidth=2,
            alpha=0.8,
            label="Mean",
        )

        if plot_future:
            plt.fill_between(
                x_future,
                np.percentile(y_future, 5, axis=0),
                np.percentile(y_future, 95, axis=0),
                color=1 - 0.4 * (1 - np.array(matplotlib.colors.to_rgb(color_line))),
                label="95% CI",
            )
            plt.plot(
                x_future,
                np.percentile(y_future, 50, axis=0),
                color=color_line,
                linewidth=2,
                alpha=0.8,
                label="Mean",
            )

    plt.scatter(x, y, 10, color=color_scatter, label="Data")
    if plot_future:
        plt.scatter(
            x_future,
            y_future.mean(axis=0),
            20,
            color=color_scatter,
            marker="x",
            label="Future Data",
        )
    plt.xlabel("date index")
    plt.ylabel("Percentage of Positives")
    plt.legend()
    plt.show()


def get_samples(model, gp, trace, X, samples=500):
    var_name = str(np.random.rand())
    with model:
        var = gp.conditional(var_name, X.reshape(-1, 1))
        samples = pm.sample_posterior_predictive(trace, vars=[var], samples=samples)
    return samples[var_name]


def plot_residual(y, y_pred, labels=None):
    plt.figure()
    plt.title("Residual Plot")
    if type(y_pred) is list:
        for i, y_p in enumerate(y_pred):
            residuals = np.abs(y - y_p)
            error = residuals.sum()
            plt.bar(
                np.arange(len(residuals)),
                height=residuals,
                label=f"{labels[i]}, total error: {error:.3f}",
                alpha=1 / len(y_pred),
            )
    else:
        residuals = np.abs(y - y_pred)
        plt.bar(np.arange(len(residuals)), height=residuals)
    plt.legend()
    plt.show()
